import subprocess
import re
import sys
import os
import traceback
import threading
import time


zabbix_server = '127.0.0.1'
datafile = '/tmp/zabbix_sender_values'
zabbix_command = '/usr/local/zabbix_server/bin/zabbix_sender -T -vv -z ' + zabbix_server + ' -i ' + datafile
container_list_file = '/tmp/docker.container.list'
container = {}
container_list_state = {}

def run():
    try:
        process = subprocess.Popen("/usr/bin/docker stats", stdout=subprocess.PIPE, shell=True)
        while True:
            line = process.stdout.readline().strip()
            if not line:
                break
            yield line
    except:
        print('Error while start docker run command in function run(). Error: '+traceback.format_exc())

def line_parse(line):
    try:
        if debug:
            print(" ")
            print("parsing line: " +line)
        line = line.replace("/", '')
        line = re.sub(" +", 'xxx', line)
        return line.split('xxx')
    except:
        print('Error while pasing data in function line_parse(line). Error: '+traceback.format_exc())

def values_parse(data):
    cnt=1
    dat = {}

    try:
        for set in data:
            cnt += 1
            if cnt == 3:
                if debug:
                    print("Found container name on position " + str(cnt) + ": " + set )
                dat['docker.name'] = set
            if cnt == 4:
                if debug:
                    print("Found cpu load on position " + str(cnt) + ": " + set )
                dat['docker.cpu'] = to_number('docker.cpu', set)
            if cnt == 5:
                if debug:
                    print("Found mem usage on position " + str(cnt) + ": " + set )
                dat['docker.mem'] = to_number('docker.mem', set)
            if cnt == 7:
                if debug:
                    print("Found mem percent usage on position " + str(cnt) + ": " + set )
                dat['docker.mem.percent'] = to_number('docker.mem.percent', set)
            if cnt == 8:
                if debug:
                    print("Found net in usage on position " + str(cnt) + ": " + set )
                dat['docker.net.in'] = to_number('docker.net.in', set)
            if cnt == 9:
                if debug:
                    print("Found net out usage on position " + str(cnt) + ": " + set )
                dat['docker.net.out'] = to_number('docker.net.out', set)
            if cnt == 10:
                if debug:
                    print("Found block in usage on position " + str(cnt) + ": " + set )
                dat['docker.block.in'] = to_number('docker.block.in', set)
            if cnt == 11:
                if debug:
                    print("Found block out usage on position " + str(cnt) + ": " + set )
                dat['docker.block.out'] = to_number('docker.block.out', set)

        return dat

    except:
        print('Error while pasing data in function values_parse(data). Error: '+traceback.format_exc())
    
def to_logfile(data):
    # I am not able to get zabbix_sender with the --real-time option to run on alpine linux docker container 
    # so write the values to file and start zabbix_sender with file input every 60 seconds
    f = open(datafile, "a")
    
    name = data['docker.name']
    for set in data:
        if 'docker.name' not in set:
            if container[name][set] != data[set]:
                container[name][set] = data[set]
                if name and set and data[set]:
                    v = '"'+name+'" ' + set + ' ' + str(int(time.time())) + ' ' + data[set] + '\n'
                    if debug:
                        print( "Set new value in " + name + ' - ' + set + "value " + data[set])
                        print("Write to logfile: ", v)
                    
                    try:
                        f.write( v )
                        if debug:
                            print("Successful writing to logfile: ", v)

                    except:
                        print('Error while write data in function to_logfile(data). Error: '+traceback.format_exc())

def to_number(key, data):
    found = False
    val = 0
    if re.search(r"(.+)(%)", data):
        found = True
        r = re.search(r"(.+)(%)", data)
        value = float(r.group(1))
        unit = r.group(2)
        if debug:
            print("Found percent sign in value " + key)
            print(r.group(1), r.group(2))
    elif re.search(r"(.*\d)([a-zA-Z]+)", data):
        found = True
        r = re.search(r"(.*\d)([a-zA-Z]+)", data)
        value = float(r.group(1))
        unit = r.group(2)
        if debug:
            print("Found letter(s) in value " + key)
            print(r.group(1), r.group(2))
    
    if not found:
        if debug:
            print("Did not find percent or unit values in data " + key)
        return False

    if unit == 'B':
        if debug:
            print("Value is already in byte. Return " + str(value))
    if unit == 'kB':
        value = value * 1024
        if debug:
            print("Value is in kB. Return " + str(value) + ' byte')
    if unit == 'MB' or unit == 'MiB':
        value = value * 1024 * 1024
        if debug:
            print("Value is in MB. Return " + str(value) + ' byte')
    if unit == 'GB' or unit == 'GiB':
        value = value * 1024 * 1024 * 1024
        if debug:
            print("Value is in GB. Return " + str(value) + ' byte')
    
    return str(int(value))


def data_process():
    global container
    global container_list_state
    if os.path.getsize(datafile) != 0:
        try:
            subprocess.check_output(zabbix_command, shell=True)
            if debug:
                print("Found data in data file. Send them to zabbix_server with command :")
                print(zabbix_command)
        except subprocess.CalledProcessError as e:
            print('Error while submitting data to zabbix_sender. Error: '+e.output)

    # restart myself in 60 seconds
    timer = threading.Timer(60.0, data_process)
    timer.start()

    # empty the data log file
    open(datafile, "w")

    #create a list of running container on that server. You need it if the server crash. 
    container_list_cur = ''
    container_to_delete = {}
    for c in container:
        container_list_cur = container_list_cur + c + '|'
        container_list_state[c]['cnt']+= 1
        if debug:
            print('COUNTER', c, container_list_state[c]['cnt'])

        if container_list_state[c]['cnt'] > 9:
            if debug:
                print('Container ' + c + 'is inactive since 10 Minutes. Add it to deletion list')
            # python does not like the deletion from an dictionary item in a loop over the dict. 
            # So mark it as to delete
            container_to_delete[c] = True
        
    for cd in container_to_delete:
        if debug:
                print('Container ' + cd + 'is removed from container list')
        del( container[cd] )

    #write list to file
    c = open(container_list_file, "w")
    c.write(container_list_cur)

    # compare current running containers with latest state
    # reset container list if a new container is added
    if container_list_cur != container_list_state['container_string']:
        if debug:
            print('A new container starts on this machine. Reset container list')
            print('Old list' + container_list_state['container_string'])
            print('New list' + container_list_cur)
        container = {}
        container_list_state['container_string'] = container_list_cur


if __name__ == "__main__":
    debug = False
    if 'debug' in sys.argv:
        debug = True
        print("Start parsing lines. Lines are truncated and send to zabbix_sender")

    cnt = 1
    container_list_state['container_string'] = ''

    # create the data files
    open(datafile, "w")
    open(container_list_file, "w")

    # start repeating function for data processing
    data_process()

    for line in run():
        line = line.decode("utf-8")
        # output from docker stats first line contain header with item names
        r = re.search(r"CONTAINER", line)
        if not r:
            cnt += 1
            values = values_parse(line_parse(line))
            container_name = values['docker.name']
            if container_name not in container:
                container[container_name] = {}
                container_list_state[container_name] = {}

                if debug:
                    print('Found new container: ' + container_name)
                    print('Add items to new container')
                
                for item in values:
                    if item != 'docker.name':
                        container[container_name][item] = 0
                if debug:
                    print(container[container_name])

            # reset container alive counter
            container_list_state[container_name]['cnt'] = 1
            to_logfile(values)
                
            if debug:
                print(" ")
                print(values)

        
